# ~/.bashrc
#
# This file is sourced by all *interactive* bash shells on startup,
# including some apparently interactive shells such as scp and rcp that
# can't tolerate any output.  So make sure this doesn't display anything
# or bad things will happen!

# Test for an interactive shell.  There is no need to set anything past
# this point for scp and rcp, and it's important to refrain from outputting
# anything in those cases.
if [[ $- != *i* ]]; then
	# Shell is non-interactive.  Be done now!
	return
fi

# Bash won't get SIGWINCH if another process is in the foreground.
# Enable checkwinsize so that bash will check the terminal size when it
# regains control.
shopt -s checkwinsize

# Disable completion when the input buffer is empty.  i.e.  Hitting tab
# and waiting a long time for bash to expand all of $PATH.
shopt -s no_empty_cmd_completion

# Enable history appending instead of overwriting when exiting.
shopt -s histappend

UPDATE_PROMPT() {
	# Save the exit status of the last command.
	local E="$?"

	# Default short prompt when the shell is running in a narrow
	# console.
	PS1="\\$\[\033[;m\] "

	# Switch to the long prompt when the shell is running in a
	# wide console.
	if ((COLUMNS >= 80)); then
		PS1="\h\[\033[;34;1m\] \$PWD $PS1"
	fi

	# Prepend the exit status to the prompt when the exit status of
	# the last command is non-zero.
	if ((E)); then
		PS1="\$? $PS1"
	fi

	# Print the prompt in red when the shell is running as root,
	# otherwise print the prompt in green.
	if ((EUID)); then
		PS1="\[\033[;32;1m\]$PS1"
	else
		PS1="\[\033[;31;1m\]$PS1"
	fi

	# Change the terminal window title when the shell is running in
	# a terminal emulator.
	if [[ $TERM == xterm* ]]; then
		PS1="\[\033]0;\u@\h:\w\007\]$PS1"
	fi

	# Return the exit status of the last command.
	return "$E"
}

# Update the primary prompt prior to issuing each primary prompt.
PROMPT_COMMAND=(UPDATE_PROMPT)

alias ls="command ls --color=auto"
alias grep="command grep --color=auto"

for BASHRC in ~/.bashrc.d/*.sh; do
	if [ -f "$BASHRC" ]; then . "$BASHRC"; fi
done

# Try to keep environment pollution down, EPA loves us.
unset BASHRC
